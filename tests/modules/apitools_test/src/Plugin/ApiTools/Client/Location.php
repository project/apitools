<?php

namespace Drupal\apitools_test\Plugin\ApiTools\Client;

/**
 * @ApiToolsObject(
 *   id = "apitools_test_client_location",
 *   type = "client",
 *   api = "apitools_test",
 *   machine_name = "location",
 *   label = @Translation("Location"),
 *   client_properties = {
 *     "location": {
 *       "get": "customer/{location_id}",
 *     }
 *   },
 *   object_properties = {
 *     "mxmerchant_model_payment": {
 *       "getAll": "customer/{customer_id}/payment"
 *     },
 *     "mxmerchant_model_address": {
 *       "getAll": "customer/{customer_id}/address",
 *       "get": "customer/{customer_id}/address/{address_id}"
 *     },
 *     "mxmerchant_model_card_account": {
 *       "getAll": "customer/{customer_id}/cardaccount",
 *       "get": "customer/{customer_id}/cardaccount/{card_account_id}"
 *     }
 *   }
 * )
 */
class Location {

}