<?php

namespace Drupal\apitools_test\Plugin\ApiTools\Client;

use Drupal\apitools\ClientObject;

/**
 * @ApiToolsObject(
 *   id = "apitools_test_client_customer",
 *   type = "client",
 *   api = "apitools_test",
 *   machine_name = "customer",
 *   label = @Translation("Customer"),
 *   controller = "\Drupal\commerce_mxmerchant\Plugin\ApiTools\Controller\Customer",
 *   client_properties = {
 *     "customers": {
 *       "getAll": "customer",
 *       "get": "customer/{customer_id}",
 *     }
 *   },
 *   model_properties = {
 *     "mxmerchant_model_payment": {
 *       "getAll": "customer/{customer_id}/payment"
 *     },
 *     "mxmerchant_model_address": {
 *       "getAll": "customer/{customer_id}/address",
 *       "get": "customer/{customer_id}/address/{address_id}"
 *     },
 *     "mxmerchant_model_card_account": {
 *       "getAll": "customer/{customer_id}/cardaccount",
 *       "get": "customer/{customer_id}/cardaccount/{card_account_id}"
 *     }
 *   }
 * )
 */
class Customer extends ClientObject {
  
  protected $user;

  protected $profile;

  public function getName() {
    return isset($this->data['name']) ? $this->data['name'] : '';
  }

  public function setUser($user) {
    $this->user = $user;
  }

  public function setProfile($profile) {
    $this->profile = $profile;
  }

  public function getProfile() {
    if (!isset($this->profile)) {
      $this->profile = FALSE;
      $user = $this->getUser();
      $profile_storage = \Drupal::service('entity_type.manager')->getStorage('profile');
      $profiles = $profile_storage->loadMultipleByUser($user, 'customer');
      foreach ($profiles as $profile) {
        if ($profile->field_type->getString() == 'personal') {
          $this->profile = $profile;
        }
      }
    }
    return $this->profile;
  }

  public function getUser() {
    if (!isset($this->user) && $this->id) {
      $this->user = $this->controller->getUserByCustomerId($this->id);
    }
    return $this->user;
  }

  protected function doInsert() {
    if (!$profile = $this->getProfile()) {
      \Drupal::service('messenger')->addError(t('No personal profile found.'));
      return;
    }
    $address = $profile->address->first()->getValue();
    $values = [];
    foreach (Address::getAddressMapping() as $drupal_key => $mx_key) {
      $values[$mx_key] = $address[$drupal_key];
    }
    $response = $this->controller->getClient()->post('customer', [
      'json' => $values,
    ]);
    $location = $response->getHeader('Location');
    $location = reset($location);
    $id = str_replace($this->controller->getClient()->url('customer') . '/', '', $location);
    $this->user->commerce_remote_id->setValue([
      'provider' => $this->controller->getClient()->getProviderName(),
      'remote_id' => $id, 
    ]);
    $this->user->save();
    $this->id = $id;
    if ($address = $this->getPrimaryAddress()) {
      $profile = $this->getProfile();
      $profile->set('commerce_remote_id', [
        'remote_id' => $address->id,
        'provider' => $this->controller->getClient()->getProviderName(),
      ]);
      $profile->save();
    }
    return $this;
  }

  protected function doUpdate() {
    $user = $this->getUser(); 
    $profile = $this->getProfile();
    $mapping = Address::getAddressMapping('customer');
    $address = $profile->address->first()->getValue();
    $values = [];
    foreach ($mapping as $drupal_key => $mx_key) {
      $values[$mx_key] = $address[$drupal_key];
    }
    // TODO: OUTLINE
    // on first save, the address and the core customer data are identical, but this needs to be clearer
    // on update, the address fields cannot be updated, they have to actually be updated with a separate address call,
    // but the core fields can still be updated on the customer update alone.
    // this means that technically the PERSONAL profile is going to be handled differently than the BILLING profiles, and that needs to be clearer as well.
    $response = $this->controller->getClient()->put('customer/' . $this->id, [
      'json' => $values,
    ]);
  }

  public function save() {
    if (!$this->id) {
      return $this->doInsert();
    }
    else {
      return $this->doUpdate();
    }
  }

  public function delete() {
    if (!$this->id) {
      return FALSE;
    }
    return $this->controller->getClient()->delete('customer/' . $this->id);
  }

  public function getPrimaryAddress() {
    $default_address = array_filter($this->addresses->getAll(), function($address) {
      return $address->isPrimary();
    });
    return !empty($default_address) ? reset($default_address) : [];
  }

}
