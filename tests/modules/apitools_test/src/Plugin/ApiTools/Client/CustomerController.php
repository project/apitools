<?php

namespace Drupal\apitools_test\Plugin\ApiTools\Client;

use Drupal\apitools\ClientObjectControllerDefault;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\UserInterface;

class CustomerController extends ClientObjectControllerDefault {

  protected function doCreate(array $values = []) {
    $customer = parent::doCreate($values);
    // Create from connected user account.
    if (!empty($values['user'])) {
      /** @var UserInterface $user **/
      $user = $values['user'];
      $customer->setUser($user);
    }
    // Create from personal profile object.
    if (!empty($values['profile'])) {
      /** @var ProfileInterface $profile **/
      $profile = $values['profile'];
      $customer->setProfile($profile);
    }
    return $customer;
  }

  public function createFromUser(UserInterface $user) {
    return $this->create(['user' => $user]);
  }

  public function getByUser(UserInterface $user, array $options = []) {
    if (empty($user->commerce_remote_id->remote_id)) {
      return FALSE;
    }
    $customer_id = $user->commerce_remote_id->remote_id;
    return $this->get($customer_id, $options);
  }

  public function getByUid($uid, array $options = []) {
    if (!$user = \Drupal\user\Entity\User::load($uid)) {
      return FALSE;
    }
    return $this->getByUser($user);
  }

  public function getByProfile(ProfileInterface $profile) {
    if ($user = $profile->uid->entity) {
      return $this->getByUser($user);
    }
    return FALSE;
  }


  public function getIdByUser(UserInterface $user, array $options = []) {
    return $user->commerce_remote_id->remote_id;
  }

  public function getIdByProfile(ProfileInterface $profile, array $options = []) {
    if ($user = $profile->uid->entity) {
      return $this->getIdByUser($user, $options);
    }
    return FALSE;
  }

  public function getIdByUid($uid, array $options = []) {
    if ($user = \Drupal\user\Entity\User::load($uid)) {
      return $this->getIdByUser($user, $options);
    }
    return FALSE;
  }

  public function getUserByCustomerId($customer_id) {
    $user_storage = \Drupal::service('entity_type.manager')->getStorage('user');
    $users = $user_storage->loadByProperties([
      'commerce_remote_id' => $customer_id,
    ]);
    return !empty($users) ? reset($users) : FALSE;
  }

  public function export() {
    // If we ever need this, process these into models.
    return $this->request('get', 'customer/export');
  }

}
