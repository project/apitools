<?php

namespace Drupal\apitools_test\Plugin\ApiTools;

use Drupal\apitools\ClientBase;
use Drupal\apitools\ClientManagerInterface;
use Drupal\apitools\ObjectManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\TempStore\SharedTempStore;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Creates a new SurveyMonkey Apply api client.
 *
 * @ApiToolsClient(
 *   id = "apitools_test_client",
 *   admin_label = @Translation("API Tools Client"),
 *   api = "apitools_test",
 * )
 */
class Client extends ClientBase {

    /**
     * @var String
     */
    protected $providerName;

    protected $gateway;

    private $consumerKey;

    private $consumerSecret;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientManagerInterface $client_manager, ObjectManagerInterface $model_manager) {
      parent::__construct($configuration, $plugin_id, $plugin_definition, $client_manager, $model_manager);
    }

  public function init(array $options = []) {
    $url = \Drupal::request()->getSchemeAndHttpHost();

    $this->options->add([
      'base_uri' => $url,
      'base_path' => '',
    ]);

    return parent::init($options);
  }

  /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
      return new static(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->get('plugin.manager.apitools_client'),
        $container->get('plugin.manager.apitools_object')
      );
    }

    public function setGateway(PaymentGatewayInterface $gateway) {
      $this->gateway = $gateway;
      // get payment gateway from storage
      // find out if it's enabled
      // get test/production mode
      // get auth settings, key and what not
      // create a new mxmerchant client from that.
      $configuration = $gateway->getPluginConfiguration();

      $this->params = new \Drupal\apitools\Utility\ParameterBag([
        'oauth_callback' => '',
        'oauth_consumer_key' => '',
        'oauth_nonce' => sha1(microtime()),
        'oauth_signature_method' => 'HMAC-SHA1',
        'oauth_timestamp' => time(),
        'oauth_version' => '1.0'
      ]);

      $key_manager = \Drupal::service('key.repository');

      $consumer_key = $key_manager->getKey($configuration['consumer_key']);
      $key_values = $consumer_key->getKeyValues();
      if (!empty($key_values['consumer_key'])) {
        $consumer_key = $key_values['consumer_key'];
      }
      else {
        $consumer_key = $consumer_key->getKeyValue();
      }

      $consumer_secret = $key_manager->getKey($configuration['consumer_secret']);
      $key_values = $consumer_secret->getKeyValues();
      if (!empty($key_values['consumer_secret'])) {
        $consumer_secret = $key_values['consumer_secret'];
      }
      else {
        $consumer_secret = $consumer_secret->getKeyValue();
      }

      // Also check the getKeyValue to see if it actually has a proper value.
      if (!$consumer_key || !$consumer_secret) {
        return FALSE;
      }
      $this->consumerKey = $consumer_key;
      $this->params->set('oauth_consumer_key', $this->consumerKey);
      $this->consumerSecret = $consumer_secret;
      $this->tempStore = $this->manager->getTempStore($gateway->id());
      $this->httpClient = $this->manager->getClientFactory()->fromOptions($this->options->all());
      $this->providerName = $gateway->id();
      //$this->oauth($consumer_key, $consumer_secret);
      return $this;
    }

    public function setGatewayId($gateway_id) {
      if ($this->gateway && $this->gateway->id() == $gateway_id) {
        // Skip as this is already set and we don't need to re-authenticate.
        return $this;
      }
      if (!$gateway = $this->gatewayStorage->load($gateway_id)) {
        throw new \Exception('No gateway found.');
      }
      $this->setGateway($gateway);
      return $this;
    }

    public function getProviderName() {
      return $this->providerName;
    }

//    public function oauth($key, $secret) {
//      $this->consumerKey = trim($key);
//      $this->consumerSecret = trim($secret);
//      return $this;
//    }

//    public function request($method, $path, $options = []) {
//      $this->options->add($options);
//      $path = $this->options->get('base_path') . '/' . $path;
//      $this->signature($path, strtoupper($method));
//      $parameters = [
//        'headers' => [
//          'Authorization' => 'Bearer ',
//        ],
//      ];
//      if ($this->options->get('json')) {
//        $parameters['json'] = $this->options->get('json');
//      }
//      $response = $this->httpClient->{$method}($path, $parameters);
//      return $response;
//    }


    protected function getTokens() {
      $token_access = $this->tempStore->get('oauth_access_token');
      $token_refresh = $this->tempStore->get('oauth_refresh_token');
      return !empty($token_access) && !empty($token_refresh) ? $this->tempStore : FALSE;
    }

    protected function setTokens(array $tokens = []) {
      foreach ($tokens as $type => $token) {
        $this->tempStore->set($type, $token);
      }
      return $this->tempStore;
    }

    protected function clearTokens() {
      $this->tempStore->delete('oauth_access_token');
      $this->tempStore->delete('oauth_refresh_token');
      return $this->tempStore;
    }

//    protected function loadTokens() {
//      $token = $this->tempStore->get('oauth_token');
//      $token_secret = $this->tempStore->get('oauth_token_secret');
//      return !empty($token) && !empty($token_secret) ? $this->tempStore : FALSE;
//    }
//
//    protected function saveTokens() {
//      $token = $this->params->get('oauth_token');
//      $token_secret = $this->options->get('oauth_token_secret');
//
//      $this->tempStore->set('oauth_token_secret', $token_secret);
//      // Set to params to go straight to the header.
//      $this->tempStore->set('oauth_token', $token);
//      return $this;
//    }
//    protected function setTokens($token, $secret) {
//      // Set to options to be used to generate the signature.
//      $this->options->set('oauth_token_secret', $secret);
//      // Set to params to go straight to the header.
//      $this->params->set('oauth_token', $token);
//      return $this;
//    }

    public function refresh() {
      $this->tempStore->delete('oauth_token');
      $this->tempStore->delete('oauth_token_secret');
      return $this;
    }

  public function oauth() {
    $this->auth();
  }

  protected function auth() {
    $url = \Drupal::request()->getSchemeAndHttpHost();
    $client_id = '81057431-51b8-4f82-bf27-147093ec6f62';
    $username = 'admin';
    $password = 'admin';
    if (!$tokens = $this->getTokens()) {
      $options = [
        'form_params' => [
          'grant_type' => 'password',
          'client_id' => $client_id,
          'username' => $username,
          'password' => $password,
        ],
      ];
      $response = $this->request('post', 'oauth/token', $options);
      $response = Json::decode($response);
      if (!empty($response['access_token']) && !empty($response['refresh_token'])) {
        $tokens = $this->setTokens([
          'oauth_access_token' => $response['access_token'],
          'oauth_refresh_token' => $response['refresh_token'],
        ]);
      }
    }

    $options = [
      'headers' => [
        'Authorization' => 'Bearer ' . $tokens->get('oauth_access_token'),
      ],
      'form_params' => [
        'grant_type' => 'refresh_token',
        'client_id' => $client_id,
        'refresh_token' => $tokens->get('oauth_refresh_token'),
      ],
    ];
    if (!$response = $this->request('post', 'oauth/token', $options)) {
      $this->clearTokens();  
    }
    ksm($response);
  }
//    protected function auth() {
//      if ($tokens = $this->loadTokens()) {
//        return $this->setTokens($tokens->get('oauth_token'), $tokens->get('oauth_token_secret'));
//      }
//
//      foreach (['request', 'access'] as $type) {
//        $response = $this->request('post', 'oauth/1a/' . $type . 'token')
//          ->getBody()->getContents();
//        $array = [];
//        // Response is in the form of a url query string, so we need to parse.
//        parse_str($response, $array);
//        $this->setTokens($array['oauth_token'], $array['oauth_token_secret']);
//      }
//      $tihs->params->add([
//        'headers' => [
//          'Authorization' => 'Bearer ' . $response['access_token'],
//        ],
//      ]);
//      return $this->saveTokens();
//    }

    protected function signature($path, $method = 'POST') {
      $this->params->remove('oauth_signature');
      $url = $this->options->get('base_uri') . $path;

      $base_string = implode('&', [
        $method,
        $this->encode($url),
        $this->encode($this->params->query())
      ]);

      $key = $this->encode($this->consumerSecret) . '&';

      if ($secret = $this->options->get('oauth_token_secret')) {
        $key .= $secret;
      }
      $this->params->set('oauth_signature', base64_encode(hash_hmac('sha1', $base_string, $key,true)));
      return $this;
    }

    public function basicTest() {
      $response = $this->client->get('/checkout/v3/customer', [
        'query' => [
          'merchantId' => $this->options->get('merchantId'),
        ],
        'auth' => ['alan.sherry', '9nVxdCLG@cdH'],
      ]);

      ksm($response->getBody()->getContents());

    }

    protected function encode($str) {
      return str_replace('+',' ',str_replace('%7E','~',rawurlencode($str)));
    }

}
