<?php

namespace Drupal\apitools_test\Plugin\ApiTools\Response;

use Drupal\apitools\ResponseObject;

/**
 * Class CustomerInterest
 *
 * @ApiToolsObject(
 *   id = "apitools_test_response_customer_interest",
 *   label = "Customer Interest",
 *   type = "response",
 *   base_entity_type = "node",
 *   base_entity_bundle = "customer_interest"
 * )
 */
class CustomerInterest extends ResponseObject {

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    return [
      'title',
      'duration',
    ]; 
  }

  protected function setDuration($value) {
    $this->entity->set('field_duration_years', $value);
  }
}
