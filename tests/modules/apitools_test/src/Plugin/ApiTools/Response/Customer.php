<?php

namespace Drupal\apitools_test\Plugin\ApiTools\Response;

use Drupal\apitools\ResponseObject;
use Drupal\apitools\ResponseObjectManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Client
 *
 * Fields:
 * Referred Other field_referred_other  Text (plain)  
 * Referred Ad  field_referred_ad Text (plain)  
 * Referred field_referred  List (text) 
 *
 * @ApiToolsObject(
 *   id = "apitools_test_response_customer",
 *   label = "Customer",
 *   type = "response"
 * )
 */
class Customer extends ResponseObject {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.response_object'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    return [
      'name',
    ];
  }

  public function getId() {
    return $this->client->id();
  }

  protected function getName() {
    $first_name = $this->getFirstName();
    $last_name = $this->getLastName();
    return join(' ', [$first_name, $last_name]);
  }

  protected function getFirstName() {
    return $this->client->getAddressFieldValue('given_name');
  }

  protected function getLastName() {
    return $this->client->getAddressFieldValue('family_name');
  }

  protected function getFolder() {
    if ($order = $this->client->getOrder()) {
      return $order->getOrderNumber();
    }
    return NULL;
  }

  protected function getEmail() {
    return $this->client->getEmail();
  }

  protected function getSex() {
    if (!$details = $this->client->getProfile('details')) {
      return FALSE;
    }
    return $details->field_sex->getString();
  }

  protected function getPronouns() {
    if (!$details = $this->client->getProfile('details')) {
      return NULL;
    }
    return $details->field_preferred_pronoun->getString();
  }

  protected function getFamily() {
    if (!$details = $this->client->getProfile('details')) {
      return NULL;
    }
    return $this->objectManager->createInstance('client_family', [
      'entity' => $details,
    ]);
  }

  protected function setFamily($values) {
    $client_family = $this->objectManager->createInstance('client_family', [
      'entity' => $this->client->details,
      'values' => $values,
    ]);
    $this->relatedObjects[] = $client_family;
    return $client_family;
  }

  protected function setLocation($values) {
    // TODO: Make sure this is a valid location nid.
    if (!$order = $this->client->getOrder()) {
      // Sets location for order and adds order to client.
      $order = $this->client->createOrder($values);
    } 
    $this->client->getUser()->set('field_location', $values);
  }

  protected function getLocation() {
    if (!$order = $this->client->getOrder()) {
      return NULL;
    }
    return $order->field_location->entity ? $order->field_location->entity->id() : NULL;
  }

  protected function setEducation($values) {
    return $this->objectManager->createInstance('client_education', [
      'entity' => $this->client->details,
      'values' => $values,
    ]);
  }

  protected function getEducation() {
    if (!$details = $this->client->getProfile('details')) {
      return NULL;
    }
    return $this->objectManager->createInstance('client_education', [
      'entity' => $details,
    ]);
  }

  protected function getPreferred() {
    if (!$details = $this->client->getProfile('details')) {
      return FALSE;
    }
    return FALSE;
    return $details->field_preferred_contact->getString();
  }

  protected function getAddress() {
    $address = [];
    foreach ($this->getAddressMap() as $drupal_field => $return_field) {
      $address[$return_field] = $this->client->getAddressFieldValue($drupal_field);
    }
    return $address;
  }

  protected function getEmployment() {
    return $this->getJobs();
  }

  private function addEntityReferenceFieldObject($field, $plugin_id) {

  }
  private function setEntityReferenceFieldObjects($field, $plugin_id) {
    $jobs = [];
    foreach ($field as $item) {
      if (!$entity = $item->entity) {
        continue;
      }
      $jobs[$entity->id()] = $this->objectManager->createInstance($plugin_id, [
        'entity' => $entity,
      ]);
    }
    return $jobs;
  }

  private function getEntityReferenceFieldObjects($field, $plugin_id) {
    $jobs = [];
    foreach ($field as $item) {
      if (!$entity = $item->entity) {
        continue;
      }
      $jobs[$entity->id()] = $this->objectManager->createInstance($plugin_id, [
        'entity' => $entity,
      ]);
    }
    return $jobs;
  }

  protected function getJobs() {
    // Current         field_current         Boolean 
    // Job roles       field_job_roles       Entity reference  
    // Number of years field_duration_years  Number (integer)  
    // Rating          field_rating          List (text)

      //Rating  field_rating  List (text)
    if (!$details = $this->client->getProfile('details')) {
      return FALSE;
    }
    $jobs = [];
    foreach ($details->field_jobs as $item) {
      if (!$entity = $item->entity) {
        continue;
      }
      $jobs[$entity->id()] = $this->objectManager->createInstance('client_job', [
        'entity' => $entity,
      ]);
    }
    return $jobs;
  }

  protected function addEmployments($values) {
    return $this->addJobs($values);
  }

  protected function addJobs($values) {
    // Current         field_current         Boolean 
    // Job roles       field_job_roles       Entity reference  
    // Number of years field_duration_years  Number (integer)  
    // Rating          field_rating          List (text)
    $values += [
      'title' => $values['title'],
      'status' => 1,
    ];
    $interest = $this->objectManager->createInstance('client_job', [
      'values' => $values,
    ]);
    $this->client->details->field_jobs->appendItem($interest->getEntity());  
  }

  protected function getContacts() {
    if (!$details = $this->client->getProfile('details')) {
      return FALSE;
    }
    return $this->getEntityReferenceFieldObjects($details->field_contacts, 'client_contact');
  }

  protected function setContacts($values) {
    return [];
    if (!$details = $this->client->getProfile('details')) {
      return [];
    }
    $interests = [];
    foreach ($details->field_contacts as $item) {
      if (!$entity = $item->entity) {
        continue;
      }
      $interests[$entity->id()] = [
        'title' => $entity->label(),
        'duration' => $entity->field_duration_years->getString(),
      ];
    }
    return $interests;
  }

  protected function getInterests() {
    if (!$details = $this->client->getProfile('details')) {
      return [];
    }
    $interests = [];
    foreach ($details->field_interests as $item) {
      if (!$entity = $item->entity) {
        continue;
      }
      $interests[$entity->id()] = [
        'title' => $entity->label(),
        'duration' => $entity->field_duration_years->getString(),
      ];
    }
    return $interests;
  }

  protected function getPayments() {
    return [];
  }

  protected function getAppointments() {
    if (!$this->client->id()) {
      return [];
    }
    $appointments = $this->entityTypeManager->getStorage('appointment')->loadByProperties([
      'client' => $this->client->id(),
    ]);
    $data = array_map(function($entity) {
      $appointment = $this->objectManager->createInstance('appointment', [
        'entity' => $entity,
      ]);
      return $appointment->toArray();
    }, $appointments);
    return $data;
  }

  protected function setFirstName($value) {
    $this->client->setAddressFieldValue('given_name', $value);
      
  }

  protected function setLastName($value) {
    $this->client->setAddressFieldValue('family_name', $value);
  }

  protected function setEmail($value) {
    $this->client->setEmail($value);
  }

  protected function setSex($value) {
    $this->client->details->set('field_sex', $value);  
  }

  protected function setPronouns($value) {
    $this->client->details->set('field_preferred_pronoun', $value);  
  }

  protected function addInterests($values) {
    $values += [
      'title' => $values['title'],
      'status' => 1,
    ];
    $interest = $this->objectManager->createInstance('client_interest', [
      'values' => $values,
    ]);
    $this->client->details->field_interests->appendItem($interest->getEntity());  
  }

  public function getPaymentMethods() {
    if (!$gateway = $this->getPaymentGateway()) {
      return [];
    }

    return $this->entityTypeManager
      ->getStorage('commerce_payment_method')
      ->loadReusable($this->entity, $gateway);
  }

  /**
   * TODO: This should be 'addPaymentMethod'.
   */
  public function addPaymentMethods($values = []) {
    if (!$gateway = $this->getPaymentGateway()) {
      return [];
    }
    $payment_method = $this->entityTypeManager->getStorage('commerce_payment_method')
      ->create(array_merge([
        'payment_gateway' => $gateway,
        'type' => 'credit_card',
        'uid' => $this->entity->id(),
      ], $values));
    return $payment_method;
  }

  private function getAddressMap($flip = FALSE) {
    $map = [
      'country_code' => 'countryCode',
      'administrative_area' => 'state',
      'locality' => 'city',
      'postal_code' => 'zip',
      'address_line1' => 'street1',
      'address_line2' => 'street2',
    ];
    return $flip ? array_flip($map) : $map;
  }

  protected function setAddress(array $values = []) {
    foreach ($this->getAddressMap(TRUE) as $return_field => $drupal_field) {
      if (!isset($values[$return_field])) {
        continue;
      }
      $this->client->setAddressFieldValue($drupal_field, $values[$return_field]);
    }
  }

  public function delete() {
    $this->client->delete();
  }

  public function save() {
    $this->client->save();
    return parent::save();
  }
}
