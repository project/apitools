<?php

namespace Drupal\apitools;

use Drupal\Core\Entity\EntityInterface;

interface ResponseObjectManagerInterface {

  /**
   * Create a new ResponseObjectInterface and populate values.
   *
   * @param $plugin_id
   * @param array $values
   * @return \Drupal\apitools\ResponseObjectInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function create($plugin_id, array $values = []);

  /**
   * Create a new ResponseObjectInterface by the defined base entity.
   *
   * @param $plugin_id
   * @param EntityInterface $entity
   * @return \Drupal\apitools\ResponseObjectInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function createFromBaseEntity($plugin_id, EntityInterface $entity);

  /**
   * @param $plugin_id
   * @param $entity_id
   * @return \Drupal\apitools\ResponseObjectInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadFromBaseEntity($plugin_id, $entity_id);

  /**
   * @param $plugin_id
   * @param $entity_id
   * @return ResponseObjectInterface|false|object
   * @throws PluginException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadFromBaseEntityId($plugin_id, $entity_id);
}
