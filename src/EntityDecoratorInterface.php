<?php

namespace Drupal\apitools;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

interface EntityDecoratorInterface extends AccessibleInterface {

  /**
   * Returns the base entity id if exists.
   *
   * @return int|string|null
   */
  public function id();

  /**
   * Sets the base entity for this object.
   *
   * @param EntityInterface $entity
   *   The base entity object.
   */
  public function setEntity(EntityInterface $entity);

  /**
   * Gets the base entity for this object.
   *
   * @return EntityInterface
   */
  public function getEntity();

  /**
   * Runs validation on the base entity if applicable.
   *
   * @param bool $throw_exception
   *   Throw first exception instead of return all errors.
   */
  public function validate($throw_exception = TRUE);

  /**
   * Save base entity if applicable.
   *
   * @param bool $force
   *   Save entity and ignore validate errors.
   */
  public function save($force = FALSE);
}
