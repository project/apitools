<?php

namespace Drupal\apitools;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the ResponseObject plugin manager.
 */
class ResponseObjectManager extends DefaultPluginManager implements ResponseObjectManagerInterface {

  use DependencySerializationTrait;

  /**
   * @var EntityTypeManagerInterface;
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ApiObjectManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct('Plugin/ApiTools', $namespaces, $module_handler, 'Drupal\apitools\ResponseObjectInterface', 'Drupal\apitools\Annotation\ResponseObject');
    $this->alterInfo('apitools_response_object_info');
    $this->setCacheBackend($cache_backend, 'apitools_response_object_plugins');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function create($plugin_id, array $values = []) {
    return $this->createInstance($plugin_id, ['values' => $values]);
  }

  /**
   * {@inheritdoc}
   */
  public function createFromBaseEntity($plugin_id, EntityInterface $entity) {
    if (!$this->isBaseEntityPlugin($plugin_id)) {
      throw new PluginException('Plugin is invalid or does not implement \Drupal\apitools\EntityDecoratorInterface');
    }
    return $this->createInstance($plugin_id, ['entity' => $entity]);
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    /** @var ResponseObjectInterface $instance */
    $instance = parent::createInstance($plugin_id, $configuration);
    if ($this->isBaseEntityPlugin($plugin_id)) {
      $entity = $configuration['entity'] ?? $this->initBaseEntity($instance, $configuration);
      // TODO: Probably add an exception if the base entity could not be initiated.
      if ($entity) {
        $instance->setEntity($entity);
        // TODO: This will eventually either be used in setEntity or replace it.
        // Technically it should really be ->setBaseEntity anyway.
        if ($instance->hasContext($entity->getEntityTypeId())) {
          $instance->setContextValue($entity->getEntityTypeId(), $entity);
        }
      }
    }
    if (!empty($configuration['values'])) {
      $instance->setValues($configuration['values']);
    }
    return $instance;
  }

  /**
   * No longer used because it's not clear
   *
   * @deprecated
   *   Use loadByBaseEntityId instead.
   */
  public function load($plugin_id, $entity_id) {
    return $this->loadFromBaseEntityId($plugin_id, $entity_id);
  }

  /**
   * @param $plugin_id
   * @return false|mixed|null
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getBaseEntityTypeId($plugin_id) {
    if (!$definition = $this->getDefinition($plugin_id, FALSE)) {
      return NULL;
    }
    return $definition['base_entity_type'] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function loadFromBaseEntityId($plugin_id, $entity_id) {
    if (!$entity_type_id = $this->getBaseEntityTypeId($plugin_id)) {
      return FALSE;
    }
    if (!$entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id)) {
      return FALSE;
    }
    return $this->createInstance($plugin_id, [
      'entity' => $entity,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function loadFromBaseEntity($plugin_id, $entity) {
    $plugin = $this->createInstance($plugin_id, [
      'entity' => $entity,
    ]);
    if ($plugin->hasContext($entity->getEntityTypeId())) {
      $plugin->setContextValue($entity->getEntityTypeId(), $entity);
    }
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterDefinitions(&$definitions) {
    // TODO: Change the "base_entity_type" and "base_entity_bundle" into a context definition.
    // new ContextDefinition('node:contact', 'Node: contact', FALSE) (or TRUE if it's required)
    foreach ($definitions as &$definition) {
      if (empty($definition['base_entity_type'])) {
        continue;
      }
      $base_entity_type_id = $definition['base_entity_type'];
      $entity_type = $this->entityTypeManager->getDefinition($base_entity_type_id);
      $definition['context_definitions'][$base_entity_type_id] = new EntityContextDefinition(
        $base_entity_type_id,
        $entity_type->getLabel(),
        FALSE
      );
    }
    parent::alterDefinitions($definitions);
  }

  /**
   * @param ResponseObjectInterface $instance
   * @param $configuration
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function initBaseEntity(ResponseObjectInterface $instance, $configuration) {
    $definition = $instance->getPluginDefinition();
    // If the instance definied a base entity type and it wasn't provided in the parameters.
    if (!empty($definition['base_entity_type']) && !$instance->getEntity()) {
      $entity_type_id = $definition['base_entity_type'];
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      // Pass all values to the base entity directly with default values set in plugin definition.
      $values = array_merge($definition['base_entity_default_values'] ?? [], $configuration['values'] ?? []);
      if (!empty($definition['base_entity_bundle'])) {
        $bundle = $definition['base_entity_bundle'];
        $bundle_key = $entity_type->getKey('bundle');
        $values[$bundle_key] = $bundle;
      }
      return $this->entityTypeManager->getStorage($entity_type_id)->create($values);
    }
  }

  /**
   * @param $plugin_id
   * @return bool
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function isBaseEntityPlugin($plugin_id) {
    $definition = $this->getDefinition($plugin_id, FALSE);

    $interfaces = class_implements($definition['class']);
    return in_array(EntityDecoratorInterface::class, $interfaces);
  }
}
