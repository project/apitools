<?php

namespace Drupal\apitools;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Model plugins.
 *
 * @deprecated
 */
interface ModelInterface extends PluginInspectionInterface {
}
