<?php

namespace Drupal\apitools;

interface ClientObjectManagerInterface {

  /**
   * Create a new ApiTools Model object.
   *
   * @param $model_name
   *   The plugin id for the ApiTools Model.
   * @param array $values
   *   An array of data associated with the ApiTools Model.
   *
   * @return ClientObjectInterface|bool
   */
  public function getModel($model_name, array $values = []);

  /**
   * @param $model_name
   *   The plugin id for the ApiTools Model.
   *
   * @return ClientObjectControllerInterface|bool
   */
  public function getModelController($model_name);

  /**
   * @param $client_method
   *   The machine name used for the client method.
   *
   * @return ClientObjectControllerInterface|bool
   */
  public function getModelControllerByMethod($client_method);

  /**
   * Get a plugin definition by a client method.
   *
   * @param $client_method
   *   The method defined in client_properties.
   *
   * @return array|bool
   */
  public function getDefinitionByMethod($client_method);
}
