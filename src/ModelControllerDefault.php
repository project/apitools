<?php

namespace Drupal\apitools;

/**
 * Class ModelControllerDefault
 *
 * @package Drupal\apitools
 *
 * @deprecated
 */
class ModelControllerDefault extends ModelControllerBase {
  protected function doSave(ModelInterface $model) {
    // TODO: Generic doSave function if it's possible.
  }
}
