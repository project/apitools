<?php

namespace Drupal\apitools;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContextAwarePluginTrait;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Api object plugins.
 *
 * TODO: Method for createFromEntity, does a look up to find which object.
 * TODO: Distinguish between an actual object that represents an entity and one that just has a helper entity and needs to hide the ID
 */
abstract class ResponseObject extends PluginBase implements ResponseObjectInterface {

  use DependencySerializationTrait;

  use ExtensibleObjectTrait;

  use SerializableObjectTrait;

  use ContextAwarePluginTrait;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var ResponseObjectManager;
   */
  protected $objectManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ResponseObjectManager $object_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->objectManager = $object_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.response_object'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Set object with values from array if they are in the Iterator.
   *
   * @see SerializableObjectInterface::getFields()
   *
   * @param array $values
   *   Array of values, if keys are not in Iterator they are ignored.
   *
   * @return $this
   */
  public function setValues(array $values = []) {
    foreach ($this as $field_name) {
      if (!isset($values[$field_name])) {
        continue;
      }
      $this->set($field_name, $values[$field_name]);
    }
    return $this;
  }

  /**
   * Checks if the current plugin has this context defined.
   *
   * @param $name
   *   The Id of the context in context_definitions.
   *
   * @return \Drupal\Component\Plugin\Context\ContextDefinitionInterface|mixed
   *
   * @throws ContextException
   */
  public function hasContext($name) {
    try {
      $definition = $this->getContextDefinition($name);
    }
    catch (ContextException $e) {
      return FALSE;
    }
    return isset($definition) && !empty($definition);
  }
}
