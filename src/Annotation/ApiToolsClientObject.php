<?php

namespace Drupal\apitools\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ApiToolsClientObject item annotation object.
 *
 * @see \Drupal\apitools\ClientObjectManager
 * @see plugin_api
 *
 * @Annotation
 */
class ApiToolsClientObject extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Remote object or server object.
   *
   * @var string
   */
  public $type;

  /**
   * Machine name of the entity type this wraps if used.
   *
   * @var string
   */
  public $baseEntityType;

}
