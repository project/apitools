<?php

namespace Drupal\apitools\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ResponseObject annotation object.
 *
 * @deprecated Removed in 1.0 release. Replaced by
 *  \Drupal\apitools\Annotation\ApiToolsObject
 *  with the type of "server".
 *
 * @see \Drupal\apitools\ServerObjectManager
 * @see plugin_api
 *
 * @Annotation
 */
class ResponseObject extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Machine name of the entity type this wraps if used.
   *
   * @var string
   */
  public $baseEntityType;

}
