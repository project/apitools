<?php

namespace Drupal\apitools;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;

/**
 * Defines an interface for ResponseObject plugins.
 */
interface ResponseObjectInterface extends SerializableObjectInterface, ContainerFactoryPluginInterface, ContextAwarePluginInterface {

  /**
   * Serialize the current object with arrayiterator.
   *
   * @return array
   */
  public function toArray();
}
