<?php

namespace Drupal\apitools;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Http\ClientFactory as HttpClient;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\TempStore\SharedTempStore;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\apitools\Utility\ParameterBag;

/**
 * Class ClientBase.
 */
abstract class ClientBase extends PluginBase implements ClientInterface {

  /**
   * @var SharedTempStore
   */
  protected $tempStore;

  /**
   * @var ParameterBag
   */
  protected $params;

  /**
   * @var ParameterBag
   */
  protected $options;

  /**
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * @var CLientObjectManagerInterface
   */
  protected $modelManager;

    /**
     * @var ClientManagerInterface
     */
  protected $manager;

  protected $apiName;

  protected $controllers;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientManagerInterface $client_manager, ClientObjectManagerInterface $model_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->manager = $client_manager;
    $this->modelManager = $model_manager;
    $this->apiName = !empty($plugin_definition['api']) ? $plugin_definition['api'] : NULL;
    $this->options = new ParameterBag();
    $this->params = new ParameterBag();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.apitools_client'),
      $container->get('plugin.manager.apitools_object')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function init(array $options = []) {
    if (!isset($this->tempStore)) {
      $this->tempStore = $this->manager->getTempStore($this->getPluginId());
    }
    if (!isset($this->httpClient)) {
      $this->httpClient = $this->manager->getClientFactory()->fromOptions($this->options->all());
    }
    return $this;
  }

  public function setTempStore(SharedTempStore $shared_temp_store) {
    $this->tempStore = $shared_temp_store;
    return $this;
  }

  public function __get($prop) {
    if (method_exists($this, $prop)) {
      return call_user_func_array([$this, $prop], []);
    }
    if ($controller = $this->getModelController($prop)) {
       return $controller;
    }
    if (!empty($this->pluginDefinition['client_default_controller'])) {
      return $this->modelManager->createControllerInstance($this->pluginDefinition['client_default_controller'])
        ->setClient($this)
        ->setCallerClientProperty($prop);
    }
    return FALSE;
  }

  protected function getModelController($prop) {
    if (!$class = $this->modelManager->getModelControllerByMethod($prop, $this->apiName)) {
      return NULL;
    }
    return $class->setClient($this)->setCallerClientProperty($prop);
  }

  /**
   * Authenticate the client and set tokens.
   *
   * @return $this
   */
  abstract protected function auth();

  protected function postRequest($response) {
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function request($method, $path, $options = []) {
    $response = NULL;
    $request_options = clone $this->options;
    $request_options->replace(NestedArray::mergeDeep($request_options->all(), $options));
    $path = $request_options->get('base_path') . '/' . $path;
    try {
      $response = $this->httpClient->{$method}($path, $request_options->all());
      $response = $response->getBody()->getContents();
    }
    catch (\Exception $e) {
      watchdog_exception('apitools', $e);
    }
    return $this->postRequest($response);
  }

  /**
   * {@inheritdoc}
   */
  public function put($path, $options = []) {
    return $this->auth()->request('put', $path, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function patch($path, $options = []) {
    return $this->auth()->request('patch', $path, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function get($path, $options = []) {
    return $this->auth()->request('get', $path, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function post($path, $options = []) {
    return $this->auth()->request('post', $path, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($path, $options = []) {
    return $this->auth()->request('delete', $path, $options);
  }

  public function url($path) {
    $url = $this->options->get('base_uri');
    $url .= $this->options->get('base_path');
    return $url . '/' . $path;
  }
}
