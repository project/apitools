<?php

namespace Drupal\apitools;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Provides the client object plugin manager.
 */
class ClientObjectManager extends DefaultPluginManager implements ClientObjectManagerInterface, ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * @var ClassResolverInterface
   */
  protected $classResolver;

  /**
   * @var ClientObjectControllerInterface[]
   */
  protected $controllers = [];

  /**
   * Constructs a new ModelManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ClassResolverInterface $class_resolver) {
    parent::__construct('Plugin/ApiTools', $namespaces, $module_handler, 'Drupal\apitools\ClientObjectInterface', 'Drupal\apitools\Annotation\ApiToolsClientObject');

    $this->classResolver = $class_resolver;

    $this->alterInfo('apitools_client_object_info');
    $this->setCacheBackend($cache_backend, 'apitools_client_object_plugins');
  }

  public function getDefinitionsByType($type) {
    $definitions = [];
    foreach ($this->getDefinitions() as $machine_name => $definition) {
      if (empty($definition['type'])) {
        continue;
      }
      $definition_type = $definition['type'];
      $definitions[$definition_type][$machine_name] = $definition;
    }
    return !empty($definitions[$type]) ? $definitions[$type] : [];
  }
  /**
   * {@inheritdoc}
   */
  public function getDefinitionByMethod($client_method) {
    foreach ($this->getDefinitions() as $definition) {
      if (!isset($definition['client_properties'])) {
        continue;
      }
      if (!isset($definition['client_properties'][$client_method]) && !in_array($client_method, $definition['client_properties'])) {
        continue;
      }
      return $definition;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getModel($model_name, array $values = []) {
    if ($definition = $this->getDefinition($model_name)) {
      $configuration = array_merge($definition, $values);
      return $this->createInstance($model_name, $configuration);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getModelController($model_name) {
    if ($definition = $this->getDefinition($model_name)) {
      $class = !empty($definition['controller']) ? $definition['controller'] : "Drupal\apitools\ModelControllerDefault";
      return $this->createControllerInstance($class, $definition);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getModelControllerByMethod($client_method) {
    if ($definition = $this->getDefinitionByMethod($client_method)) {
      return $this->getModelController($definition['id']);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterDefinitions(&$definitions) {
    foreach ($definitions as $plugin_id => &$definition) {
      if (!empty($definition['client_property'])) {
        $definition['client_properties'] = [$definition['client_property']];
        unset($definition['client_property']);
      }
    }
    parent::alterDefinitions($definitions);
  }

  /**
   * Create a new ModelControllerInterface object.
   *
   * @param $class
   *   Class reference string.
   * @param array $definition
   *   Plugin definition array.
   *
   * @return ClientObjectControllerInterface|bool
   */
  public function createControllerInstance($class, array $definition = []) {
    if (is_subclass_of($class, 'Drupal\apitools\ClientObjectControllerInterface')) {
      $controller = $class::createInstance($this->container, $definition);
    }
    else {
      $controller = new $class($definition, $this);
    }
    return $controller;
  }

  public function load($plugin_id, $entity_id) {
    $definition = $this->getDefinition($plugin_id);
    if (empty($definition['base_entity_type'])) {
      return FALSE;
    }
    $entity_type = $definition['base_entity_type'];
    if (!$entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id)) {
      return FALSE;
    }
    return $this->createInstance($plugin_id, [
      'entity' => $entity,
    ]);
  }

  public function loadByEntity($plugin_id, $entity) {
    return $this->createInstance($plugin_id, [
      'entity' => $entity,
    ]);
  }

  protected function initBaseEntity(ResponseObjectInterface $instance, $configuration) {
    $definition = $instance->getPluginDefinition();
    // If the instance definied a base entity type and it wasn't provided in the parameters.
    if (!empty($definition['base_entity_type']) && !$instance->getEntity()) {
      $entity_type_id = $definition['base_entity_type'];
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      // Pass all values to the base entity directly.
      $values = $configuration['values'];
      if (!empty($definition['base_entity_bundle'])) {
        $bundle = $definition['base_entity_bundle'];
        $bundle_key = $entity_type->getKey('bundle');
        $values[$bundle_key] = $bundle;
      }
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->create($values);
      $instance->setEntity($entity);
    }
  }
}
