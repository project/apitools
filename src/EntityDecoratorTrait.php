<?php

namespace Drupal\apitools;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Session\AccountInterface;

trait EntityDecoratorTrait {

  /**
   * @deprecated
   *
   * @var EntityInterface;
   */
  protected $entity;

  /**
   * @deprecated
   *
   * @var string
   */
  protected $id;

  /**
   * Get the id of the base entity if applicable.
   *
   * @return int|null|string
   *
   * $deprecated
   *   Use self::id()
   */
  public function getId() {
    return $this->id();
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    if ($entity = $this->getEntity()) {
      return $entity->id();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($entity = $this->getEntity()) {
      return $entity->access($operation, $account);
    }
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity;
    $this->id = $this->entity->id();
    if ($base_entity_type_id = $this->getBaseEntityTypeId()) {
      $this->setContextValue($base_entity_type_id, $entity);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    if ($this->entity) {
      return $this->entity;
    }
    if ($base_entity_type_id = $this->getBaseEntityTypeId()) {
      $this->entity = $this->getContextValue($base_entity_type_id);
      return $this->entity;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($throw_exception = TRUE) {
    $entity = $this->getEntity();
    if ($entity && $entity instanceof FieldableEntityInterface) {
      return $entity->validate();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save($force = FALSE) {
    if ($entity = $this->getEntity()) {
      $entity->validate();
      if ($entity instanceof EntityChangedInterface) {
        $entity->setChangedTime(\Drupal::service('datetime.time')->getRequestTime());
      }
      $entity->save();
    }
    return $this;
  }

  protected function getBaseEntityTypeId() {
    $definition = $this->getPluginDefinition();
    return !empty($definition['base_entity_type']) && $this->hasContext($definition['base_entity_type'])
      ? $definition['base_entity_type']
      : FALSE;
  }
}
