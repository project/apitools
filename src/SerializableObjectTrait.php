<?php

namespace Drupal\apitools;

trait SerializableObjectTrait {

  /**
   * {@inheritdoc}
   */
  public function getIterator(): \Traversable {
    return new \ArrayIterator($this->getFields());
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): mixed {
    return (object) $this->toArray();
  }

  /**
   * Convert object to array for serialization or api response.
   */
  public function toArray() {
    // TODO: This is dependent on the base entity trait/class. Move this.
    $array = ['id' => $this->get('id')];
    foreach ($this as $field) {
      $array[$field] = $this->get($field);
    }
    return $array;
  }
}
